﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DedicatedRepLibrary.ViewModels
{
  public  class ViewModel
    {
      #region "list of Models"

        //This is here to bring DATA from different different tables into a common page i.e. CSHTML page.
        //Creating list of data by the means of models Job and Job_Histories.  

        public List<customer> allcustomer { get; set; }
        public List<job> alljobs { get; set; }
        public List<job_histories> alljobhistories { get; set; }
      
        #endregion
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Text;
using System.Data.Entity;
using MySql.Data.MySqlClient;
using System.Data.Entity.Infrastructure;

namespace DedicatedRepLibrary
{
    public class DbOperation : System.Data.Entity.DbContext
    {
         
     scrapdbEntities db = new scrapdbEntities(); //this is for entity framework object for calling of collections.
     
    
     //this is Index Page for Customer Table.(Starting Page)
     public List<customer> SelectCustomer() 
     {
         List<customer> cs = (from c in db.customers 
                             where c.Active==true
                              select c).ToList();                    
         return cs;
     }


     //this is for Customer table table record EDIT.
     public customer SelectSingleCustomer(int id)
     {
         customer cs = (from c in db.customers
                        where c.CustomerId == id
                        select c).SingleOrDefault<customer>();
         return cs;
     }

     public void UpdateCustomer(customer cs)
     {

         cs.ModifiedDate = DateTime.Now;
         db.customers.Attach(cs);
          //db.customers.Add(cs);
         db.Entry(cs).State = EntityState.Modified;
         db.SaveChanges();
     }

        //this is for Job table table record EDIT.
     public job SelectSingleJob(int id)
     {
         job j = (from c in db.jobs
                  where c.CustomerId == id
                  select c).SingleOrDefault<job>();
         return j;
     }
    
        public void UpdateJob(job j)
        {
         j.UpdateDate = DateTime.Now;
         db.jobs.Attach(j);
         db.Entry(j).State = EntityState.Modified;
         db.SaveChanges();
        }

        //this is for Job_Histories table record EDIT.
      public job_histories SelectSingleJob_Histories(int id)
        {
         job_histories jh = (from c in db.job_histories
                             where c.JobHistoryID == id
                             select c).SingleOrDefault<job_histories>();
          return jh;
        }

     public void UpdateJob_Histories(job_histories jh)
     {
         jh.UpdateDate = DateTime.Now;
         db.job_histories.Attach(jh);
         db.Entry(jh).State = EntityState.Modified;
         db.SaveChanges();
     }




     public void CreateCustomer(customer cus)
     {
         cus.Active = true;
         db.customers.Add(cus);
         db.SaveChanges();
     }

     public void DeleteCustomer(customer cus)
     {
         cus.Active = false;
         db.Entry(cus).State = EntityState.Modified;
         db.SaveChanges();
     }


     #region ViewModel "fetch data for Source and Target Data for comparison"

     public List<job> MyData_job(int id)
     {
         List<job> j = (from c in db.jobs
                  where c.CustomerId == id
                  select c).ToList<job>();

         return j;

     }

     public List<job_histories> Mydata_job_h(int id)
     {
         DateTime s = db.job_histories.Max(p => p.HistoryDate);
         List<job_histories> jh = (from c in db.job_histories
                             where c.CustomerID == id
                            // && c.HistoryDate == s
                             select c).ToList<job_histories>();

         return jh;
     }

     #endregion

     #region " OLD DATA Fetch source & Target data for comparison"
     
        //simply fetching data from jobs tables for job comparision.
        //this is my old data. Not in use. I will consider later.
        public List<object> myjobs(int id)
     {
         List<object> jo = (from j in db.jobs
                            where j.CustomerId==id
                            select j).ToList<object>();
         return jo;
         
     }

        //simply fetching data from job_histories.
     public List<object> myjob_histories(int id)
     {
         List<object> jh = (from j in db.job_histories
                            where j.CustomerID==id
                            select j).ToList<object>();
         return jh;
     }

#endregion


     // this is a section for selecting the data for "View Details" in POPUPS
     public List<job_histories> JhViewDetails(int id)
     {
         List<job_histories> jh = (from c in db.job_histories
                                   where c.JobHistoryID == id
                                   select c).ToList<job_histories>();
         return jh;

     }
     public List<job> jViewDetails(int id)
     {
         List<job> j = (from c in db.jobs
                        where c.CustomerId == id
                        select c).ToList<job>();
         return j;
     }

    }
}

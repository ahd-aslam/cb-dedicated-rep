﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DedicatedRepLibrary;
using System.Net;
using System.Web.Helpers;
using System.Dynamic;
using System.Data;
using DedicatedRepLibrary.ViewModels;

namespace DedicatedRep.Controllers
{
    public class HomeController : Controller
    {
        DbOperation db = new DbOperation();
        ViewModel vm = new ViewModel();
        
        // GET: /Home Page/

        public ActionResult Index()
        {
            vm.allcustomer = db.SelectCustomer();
            // List<customer> cs= db.SelectCustomer();
            return View(vm);
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            customer cs = db.SelectSingleCustomer(id);
            return View(cs);
        }


        [HttpGet]
        public ActionResult JobDetails(int id)
        {
            job j = db.SelectSingleJob(id);
            return View(j);
        }

        [HttpGet]
        public ActionResult job_HistoryDetails(int id)
        {
            job_histories jh = db.SelectSingleJob_Histories(id);
            return View(jh);
        }


        #region "Create" Customer Table Record.

        [HttpGet]
        public ActionResult Create()
        {
            return View();

        }

        [HttpPost]
        [ActionName("Create")]  //the actionResult of the controller identifies by the ActionName mentioned here,                               
        //if we have different actionName in the Method like:-Create_Insert().
        public ActionResult Create_Insert(customer cus)
        {
            if (ModelState.IsValid) //used to validate the fields data and datatype with models(respectively)
            {
                db.CreateCustomer(cus);//to create the Row in the Customer Table.
                return RedirectToAction("Index");//to redirect the page after operation.
            }
            return View(cus);
        }

        #endregion


        #region "Delete" of the Customer Records.
        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            customer cs = db.SelectSingleCustomer(id);
            if (cs == null)
            {
                return HttpNotFound();
            }
            return View(cs);
        }

        [HttpPost]
        [ActionName("Delete")]
        public ActionResult Delete_Customer(int id)
        {
            if (ModelState.IsValid) //used to validate the fields data and datatype with models(respectively)
            {
                customer cs = db.SelectSingleCustomer(id);
                db.DeleteCustomer(cs);//to Delete the Row in the Customer Table not physically but by making active field "False i.e. 0".
                return RedirectToAction("Index");//to redirect the page after operation.
            }
            return RedirectToAction("Index"); // to redirect the page after Operation.
        }
        #endregion


        #region "Edit" Tables Records.


        //Customer Table.
        [HttpGet]
        public ActionResult Edit(int id)
        {
            customer cs = db.SelectSingleCustomer(id);
            return View(cs);
        }

        [HttpPost]
        public ActionResult Edit(customer Cus)
        {
            if (ModelState.IsValid) //used to validate the fields data and datatype with models(respectively)
            {
                db.UpdateCustomer(Cus);
                return RedirectToAction("Index");//to redirect the page after operation.
            }
            return View(Cus);
        }

        //job Table.
        [HttpGet]
        public ActionResult EditJob(int id)
        {
            job j = db.SelectSingleJob(id);
            return View(j);
        }

        [HttpPost]
        public ActionResult EditJob(job j)
        {
            if (ModelState.IsValid)
            {
                db.UpdateJob(j);
                return RedirectToAction("SelectData");
            }
            return View(j);
        }


        //Job_Histories Table.
        [HttpGet]
        public ActionResult EditJob_Histories(int id)
        {
            job_histories jh = db.SelectSingleJob_Histories(id);
            return View(jh);
        }

        [HttpPost]
        public ActionResult EditJob_Histories(job_histories jh)
        {
            if (ModelState.IsValid)
            {
                db.UpdateJob_Histories(jh);
                return RedirectToAction("SelectData");
            }
            return View(jh);
        }


        #endregion
       

        #region " Old Testing with Fetch DATA"

        //this is deals with the data
        //public ActionResult JobCompare(int id)
        //{
        //    scrapdbEntities db = new scrapdbEntities();

        //    List<object> myModelDemo = new List<object>();
        //    myModelDemo.Add(db.jobs.ToList());
        //    myModelDemo.Add(db.job_histories.ToList());
        //    return View(myModelDemo);
        //}

        #endregion


        #region "using ViewModels" to Display Multiple Table Records.
        [HttpGet]
        public ActionResult SelectData(int id)
        {
            // DbOperation db1 = new DbOperation();

            vm.alljobs = db.MyData_job(id);
            vm.alljobhistories = db.Mydata_job_h(id);
            return View(vm);

        }
        #endregion


        #region "ViewDetails" of a record.
        //this is to see the "View Details" of the Job Table.
        [HttpGet]
        public ActionResult JhViewDetails(int id)
        {
            vm.alljobhistories = db.JhViewDetails(id);

            //  job_histories jh = db.JhViewDetails(id);
            return View(vm);
        }

        //this is to see the "View Details" of the Job_Histories Table.
        [HttpGet]
        public ActionResult JViewDetails(int id)
        {
            vm.alljobs = db.jViewDetails(id);
            return View(vm);
        }
        #endregion
   
    }
}
